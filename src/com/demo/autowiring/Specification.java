package com.demo.autowiring;

import org.springframework.beans.factory.annotation.Value;

/*

 @Author melone
 @Date 6/25/18 
 
 */
public class Specification {
    @Value("20")//@Value can be used to inject the primitive data type through annotation instead of from property tag
    private int age;
    @Value("20.5")
    private double weight;
    @Value("5.6")
    private double height;

    @Override
    public String toString() {
        return "Specification{" +
                "age=" + age +
                ", weight=" + weight +
                ", height=" + height +
                '}';
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }
}
