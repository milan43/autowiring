package com.demo.autowiring;

/*

 @Author melone
 @Date 6/25/18 
 
 */
public abstract class Animal {
    protected abstract void displayInfo();
}
