package com.demo.autowiring;

import com.demo.autowiring.domain.Crocodile;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/*

 @Author melone
 @Date 6/25/18 
 
 */
public class WiringApp {
    public static void main(String[] args) {
        ApplicationContext context = new ClassPathXmlApplicationContext("com/demo/autowiring/config/animal-config.xml");
        Animal animal = ((Animal) context.getBean("crocodile"));
        animal.displayInfo();
    }
}
