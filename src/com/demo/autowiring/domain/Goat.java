package com.demo.autowiring.domain;

import com.demo.autowiring.Animal;
import com.demo.autowiring.Specification;
import org.springframework.beans.factory.annotation.Autowired;

/*

 @Author melone
 @Date 6/25/18 
 
 */
public class Goat extends Animal {

    private String type;
    private String name;
    @Autowired(required = false)
    private Specification spec;

    public Specification getSpec() {
        return spec;
    }

    public void setSpec(Specification spec) {
        this.spec = spec;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public void displayInfo() {
        System.out.println("Name :" + name + " Type : " + type + "spec" + spec);
    }
}
