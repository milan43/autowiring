package com.demo.autowiring.domain;

import com.demo.autowiring.Animal;
import com.demo.autowiring.Specification;
import javafx.beans.NamedArg;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

/*

 @Author melone
 @Date 6/25/18 
 
 */
public class Crocodile extends Animal {
    private String type;
    private String name;

    /**
     * if any bean of name spec is found then NoSuchBeanDefinitionException is shown
     * we can set value to null if no bean found by setting autowirng to optional as
     * required = false it wires bean with null value </null>
     * if required is not set to false it shows compile time error while using @Autowired annotation
     */
    @Autowired(required = false)
    private Specification spec;

    /**
     * @Qualifier annotation is used to to match specific bean when more than one bean exist
     * the qualifier matches the value defined inside @Qualifier with qualifier deffined in xml
     */
    @Autowired(required = false)
   // @Qualifier("prio1")
    private Specification spec1;

    public Specification getSpec1() {
        return spec1;
    }

    public void setSpec1(Specification spec1) {
        this.spec1 = spec1;
    }

    public Specification getSpec() {
        return spec;
    }

    public void setSpec(Specification spec) {
        this.spec = spec;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public String getName() {
        return name;
    }

    @Override
    public void displayInfo() {
        System.out.println("Name :" + name + " Type : " + type + "spec" + spec);
    }
}
